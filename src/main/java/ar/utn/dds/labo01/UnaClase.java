package ar.utn.dds.labo01;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UnaClase {

	private List<OtraClase> unListado;

	public UnaClase() {
		this.unListado = new ArrayList<>();
	}
	
	
	
	public List<OtraClase> getUnListado() {
		return unListado;
	}



	public void aumentarNumero() {
		 this.unListado.stream().forEach(otraClase -> {
			 otraClase.setNumero(otraClase.getNumero() + 1); 
		 });
	}

	public void agregarItem(OtraClase otraClase) throws RepetidoException {
		Optional<OtraClase> existe = this.unListado.stream().filter(x -> x.getNombre().equals(otraClase.getNombre()))
				.findFirst();
		if (existe.isPresent()){
			throw new RepetidoException(otraClase.getNombre());
		} 
		this.unListado.add(otraClase);
	}

}
